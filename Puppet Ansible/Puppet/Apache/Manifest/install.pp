class apache::install {
	package { $apache::params::apache_package:
	  ensure => installed,
	}
}
