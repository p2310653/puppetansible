class apache::config {
	file { $apache::params::apache_config_file:
	  ensure  => file,
	  content => template('apache/apache2.conf.erb'),
	  require => Package[$apache::params::apache_package],
	}
}
