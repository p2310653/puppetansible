class apache::params {
     case $::osfamily {
	'Debian': {
	  $apache_package = "apache2'
	  $apache_service = 'apache2"
	  $apache_config_file = '/etc/apache2/apache2.conf"
	}
	'RedHat": {
	  $apache_service = 'httpd'
	  $apache_package = 'httpd'
	  $apache_config_file = '/etc/httpd/conf/httpd.conf
	}
	default: {
	  fail
	}
    }
}
