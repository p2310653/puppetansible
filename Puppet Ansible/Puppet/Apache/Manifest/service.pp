class apache::service {
	service { $apache::params::apache_service:
		enable	=> true,
		ensure  => running,
		require	=> Package[$apache::params::package_name],
}
}
 
