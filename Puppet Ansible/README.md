# PuppetAnsible

## Context

This project aims to automatically configure the installation of SSH and Apache services on Ubuntu and CentOS systems using Puppet and Ansible. The code is developed and maintained in a Git repository, with an organized branch structure for development, testing, and production.

## Prerequisites

- **Git**: Ensure Git is installed on your system.
- **Puppet**: Ensure Puppet is installed and configured.
- **Ansible**: Ensure Ansible is installed.
- **Ubuntu LTS**: A virtual machine to work.

## Repository Structure

The Git repository is structured as follows:
- `main`: Contains the final code and the README.md file.
- `puppet-dev`: Development of Puppet manifests.
- `puppet-prod`: Production of Puppet manifests.
- `ansible-dev`: Development of Ansible playbooks.
- `ansible-prod`: Production of Ansible playbooks.

## Puppet Manifests

The Puppet modules are organized as follows:

my_awesome_sshd/
├── manifests/
│ ├── init.pp
│ ├── params.pp
│ ├── install.pp
│ ├── config.pp
│ └── service.pp
├── files/
└── templates/
└── sshd_config.erb

### Installing all files

```
cd existing_repo
git remote add origin https://forge.univ-lyon1.fr/p2310768/puppetansible.git
git branch -M main
git push -uf origin main
```

## Puppet Environments

Puppet environments are managed using Git branches:
- `config.pp`
- `init.pp`
- `install.pp`
- `param.pp`
- `service.pp`

## Ansible Inventories

An Ansible inventory file defines hosts and groups of hosts:

[dev]
dev1.example.com
dev2.example.com

[prod]
prod1.example.com
prod2.example.com

[web:children]
dev
prod

[web:vars]
ansible_user=admin
ansible_ssh_private_key_file=/home/admin/.ssh/id_rsa

## Abstract

Puppet installs and configures a service on a host using the Puppet package. Prior to running Puppet, ensure that the Puppet package is installed on your machine. Additionally, verify that you have the necessary permissions to install and manage packages, and to execute Puppet commands. Once Puppet is installed and configured, you can create and apply Puppet manifests and modules to automate the setup and management of various services on your host. This typically involves defining the desired state of your system in Puppet code and using the Puppet agent to enforce this state, thereby ensuring consistent and reproducible configurations across your entire infrastructure.

## Comparaison of the two tools

Before comparing Puppet and Ansible, it is important to understand the purpose of these tools. Puppet and Ansible are configuration management and IT automation tools. They allow for the deployment and management of applications across multiple servers efficiently.

Ansible is much simpler and more efficient for small to medium infrastructures due to its simple and understandable commands. However, it has a major drawback in configuration management for large infrastructures (more than 500 machines), where it is easy to get lost among the many playbooks and roles. Additionally, during major updates, Ansible can encounter issues that are difficult and time-consuming to fix because its hierarchy is not practical for large-scale use.

Conversely, Puppet, although more difficult to master and requiring extensive configuration, is very effective for large infrastructures. Its strict and clear hierarchy facilitates configuration management and makes handling major updates easier. Puppet is also more performant than Ansible and is recommended for large enterprises, as its detailed hierarchy allows for easy infrastructure expansion. Ansible, on the other hand, is ideal for simple solutions and small to medium-sized enterprises.